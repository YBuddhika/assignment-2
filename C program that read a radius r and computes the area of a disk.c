#include <stdio.h>

int main()
{
  const float p = 3.14;
  int r;
  double area;

   printf( "Insert radius of disk: \n" );
   scanf("%d",&r);

   area = p*r*r;

   printf( "Area of disk:%lf",area );

   return 0;

}
